﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    public float ceilingHeight = 6f;
    public float halfWidth = 2.9f;
    public float minSpeed = 0.1f;
    public float maxSpeed = 0.5f;
    public int maxBalloons = 5;

    public bool gamePaused = false;


    private void Awake()
    {
        //register gamesettings object
        SettingsLocator.provide(this);

    }
}
