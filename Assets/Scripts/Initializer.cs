﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    public GameObject balloon;

    List<GameObject> balloonsPool = new List<GameObject>();
    GameSettings _settings;

    private void Start()
    {
        _settings = SettingsLocator.getSettings();

        BalancePool();
    }

    private void Update()
    {
        if (balloonsPool.Count < _settings.maxBalloons)
        {
            BalancePool();
        }
    }

    void BalancePool()
    {
        while(balloonsPool.Count < _settings.maxBalloons)
        {
            AddBallToPool();
        }
    }

    void AddBallToPool()
    {
        GameObject a = Instantiate(balloon, balloon.transform.position, Quaternion.identity);
        balloonsPool.Add(a);
    }
}
