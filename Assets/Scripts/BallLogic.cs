﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLogic : MonoBehaviour
{
    public delegate void OnBallTapped();
    public static event OnBallTapped onBallTapped;
//KEWK
    GameSettings _settings;
    float _speed = 0f;
    SpriteRenderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _settings = SettingsLocator.getSettings();
        Initialize(Random.Range(_settings.minSpeed, _settings.maxSpeed), Random.ColorHSV());
    }

    void Update()
    {
        if (!_settings.gamePaused)
        {
            transform.Translate(Vector3.up * _speed);
            if (transform.position.y >= _settings.ceilingHeight)
            {
                Despawn(false);
            }
        }
    }

    public void Initialize(float speed, Color color)
    {
        transform.position = new Vector3((Random.Range(-_settings.halfWidth, _settings.halfWidth)), -_settings.ceilingHeight, -1);
        _speed = speed;
        _renderer.color = color;
    }

    void Despawn(bool tapped)
    {
        if (tapped)
        {
           onBallTapped();
        }
        Initialize(Random.Range(_settings.minSpeed, _settings.maxSpeed), Random.ColorHSV());
    }

    private void OnMouseDown()
    {
        if (!_settings.gamePaused)  Despawn(true);
    }
}
