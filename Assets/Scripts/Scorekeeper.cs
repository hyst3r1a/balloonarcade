﻿using UnityEngine;
using System.Collections;
using TMPro;
public class Scorekeeper : MonoBehaviour
{
   

    TextMeshProUGUI _text;
    int _score = 0;

    void Start()
    {
        BallLogic.onBallTapped += UpdateScore;
        _text = GetComponent<TextMeshProUGUI>();
    }

    void UpdateScore()
    {
        _score += 1;
        _text.text = string.Format("Score:{0}",_score);
    }
    
}
