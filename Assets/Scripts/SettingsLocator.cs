﻿using UnityEngine;
using System.Collections;

public class SettingsLocator
{
    private static GameSettings _service;

    public static GameSettings getSettings()
    {
        if (_service != null)
        {
            return _service;
        }
        else
        {
            throw new System.ArgumentNullException("GameSettings");
        }
    }

    public static void provide(GameSettings service)
    {
        _service = service;
    }

 
}
