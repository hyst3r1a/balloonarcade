﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWindowControls : MonoBehaviour
{

    public void OpenMenu()
    {
        gameObject.SetActive(true);
        SettingsLocator.getSettings().gamePaused = true;
    }
    public void CloseMenu()
    {
        gameObject.SetActive(false);
        SettingsLocator.getSettings().gamePaused = false;
    }
}
